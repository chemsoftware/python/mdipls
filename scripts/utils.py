import numpy as np
import functions as fct
import dipals as ml
from sklearn.metrics import mean_squared_error


def performance_measures(Ytrue,  Ypred):
    
    Y = Ytrue.flatten()
    Yp = Ypred.flatten()
    
    rmsep_s = np.sqrt(mean_squared_error(Y, Yp))
    bias_s = np.mean(Y - Yp)
    r2p_s = np.power(np.corrcoef(Y.T, Yp.T)[0,1],2)
    sep_s = np.sqrt(np.mean(np.power(Y - Yp - bias_s,2)))
    
    
    return rmsep_s, bias_s, r2p_s, sep_s

def cross_val_predict_mdipls(X, Y, Xs, target_domains, max_lv,lamb, n_splits_cv = 10):
    
    
    l = [lamb]#np.array(np.repeat(lamb,lv)).astype(np.float64)
    cv_groups = np.repeat(np.arange(n_splits_cv), int(X.shape[0]/n_splits_cv)+1)[0:X.shape[0]]
    ypred_cv_mdipls = np.zeros((Y.shape[0],max_lv))

    for ii in np.unique(cv_groups):

        Xs_test = X[cv_groups==ii,:]
        Ys_test = Y[cv_groups==ii,:]
        Xs_cal = X[cv_groups!=ii,:]
        Ys_cal = Y[cv_groups!=ii,:]
        
        
        m = ml.model(Xs_cal, Ys_cal, Xs_cal, target_domains, max_lv)
        m.fit(l=l, target_domain=0)
        
        Xs_test_c = Xs_test - Xs_cal.mean(axis=0)
        b0 = np.mean(Ys_cal)
        
        for lvi in range(max_lv):
            
            b = m.W[:,:(lvi+1)]@(np.linalg.inv(m.Ps.T[:(lvi+1),:]@m.W[:,:(lvi+1)]))@m.C[:(lvi+1)]     
            
            
            Ys_test_pred = Xs_test_c@b + b0
            
            
            ypred_cv_mdipls[cv_groups==ii,lvi] = Ys_test_pred.flatten()
            

        
    return ypred_cv_mdipls


def xvar_mdipls(W,target_domains, nlv, name_domain):
    
    var_explained = {}
    
    for tt in range(len(target_domains)):
        
        var_explained["{} {:d}".format(name_domain,tt+1)] = {}
        
   
        
    for ti in range(len(target_domains)):  
        
        Xt_c = target_domains[ti] - target_domains[ti].mean(axis=0)
        xvar = np.sum(Xt_c.T.dot(Xt_c))
    
        for j in range(nlv):                   

            tt = Xt_c.dot(W[:,j:(j+1)])
            pt = (tt.T@Xt_c)/(tt.T@tt)
            Xt_hat_c = tt.dot(pt)

            vv = np.sum(Xt_hat_c.T.dot(Xt_hat_c)) / xvar
            var_explained["{} {:d}".format(name_domain,ti+1)]["% LV {:d}".format(j+1)] = np.round(100*vv,2)
            
            Xt_c = Xt_c - Xt_hat_c

    return var_explained