# mdi-PLS: Partial least squares regression with multiple domains

## Genearlization of di-PLS for multiple target domains

- Regression approach to adapt models from a single source domain to multiple target domains

- Possibility to obtain one model for all domains or specific models for every target domain

- Cross-validation strategies are documented to choose the tuning parameters


![Image taken from [1]](./figures/mango_allscores.png)

_Image taken from [1]_

## References

[1] Mikulasek B, Fonseca Diaz V, Gabauer D, Herwig C, Nikzad-Langerodi R. Partial least squares regression with multiple domains. Journal of Chemometrics. 2023;e3477. [doi:10.1002/cem.3477](doi:10.1002/cem.3477)

[2] Nikzad-Langerodi, R., Zellinger, W., Saminger-Platz, S., and Moser, B. (2019). Domain-invariant regression under Beer–Lambert’s law. In 2019 18th IEEE International Conference on Machine Learning and Applications (ICMLA),401 pages 581–586. IEEE.


